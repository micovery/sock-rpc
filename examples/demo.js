#!/usr/bin/env node

"use strict";

var rpc = require('sock-rpc');
var http = require('http');
var fs = require('fs');

rpc.register("getDate", function (callback) {
  var args = Array.prototype.slice.call(arguments, 0);
  callback = args.pop();
  callback(null, new Date().toString());
});

rpc.register("echo", function (data, callback) {
  var args = Array.prototype.slice.call(arguments, 0);
  callback = args.pop();
  data = args;
  callback(null, data);
});

/**
 * This function serves to demonstrate that it's possible to send payloads much bigger than the
 * default size of 10kb supported by callExtension buffer.
 *
 * When the message size exceeds the callExtension buffer, the response sent to SQF is simply a list of
 * chunk memory locations (e.g. {["1884c3c8","188563f8","18860428","1886a458"]})
 * which the SQF code can call back, and each of the contents of each chunk.
 *
 */
rpc.register("get32kString", function(callback) {
  fs.readFile(__dirname + "/data32k.txt", function(err, buf) {
    try {
      if (err) {
        throw err;
      }
      callback(undefined, buf);
    }
    catch(ex) {
      callback(ex);
    };
  });
});

/**
 * This function serves to demonstrate how make an HTTP request, and send the response body
 * to the SQF layer as a regular object.
 *
 * When the message is received on the SQF layer, JavaScript objects are mapped to a code block,
 * with a simple array containing the entry set (e.g. {[["key1","value1"],["key2","value2],...]})
 *
 *  geoIp([ip],callback);
 */
rpc.register("geoIp", function (ip, callback) {
  var args = Array.prototype.slice.call(arguments, 0);
  callback = args.pop();
  ip = args.shift();

  if (ip && typeof ip !== "string") {
    callback(new Error("IP address argument is not valid"));
    return;
  }

  var url = "http://www.telize.com/geoip/" + (ip || "");
  var data = new Buffer(0);

  var req = http.get(url, function (res) {
    res.on("data", function (buf) {
      data = Buffer.concat([data, buf]);
    });
  });

  req.on("error", function (ex) {
    callback(ex);
  });

  req.on("close", function () {
    try {
      var response = JSON.parse(data.toString());
      callback(null, response);
    }
    catch (ex) {
      callback(ex);
    }
  });
});

rpc.listen();