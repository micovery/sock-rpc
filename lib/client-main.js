#!/usr/bin/env node
"use strict";
var repl = require("repl"),
    util = require("util"),
    log = require("util-logging"),
    Client = require("./client.js");


var logger = new log.ConsoleLogger().setLevel(log.Level.INFO);

module.exports = function(argv) {
  var client = new Client({"host": argv.host, "port": argv.port});
  var r = repl.start("> ");
  r.context.sock_rpc = function () {
    var args = Array.prototype.slice.call(arguments, 0);
    args.push(function(err, packet) {
      console.log(packet);
    });
    client.sock_rpc.apply(client, args);
  };

  r.context.connect = function () {
    client.connect(function(err) {
      if (err) {
        logger.severe(err);
        return;
      }
      logger.info("Connected");
    })
  };

  r.context.disconnect = function () {
    client.disconnect(function(err) {
      if (err) {
        logger.severe(err);
        return;
      }
      logger.info("Disconnected");
    })
  };
};


module.exports.usage =
    "\n" +
    "Usage:\n sock-rpc-client [--host=<remote_host>] [--port=<remote_port>]\n";



module.exports.options = {
  host: {
    describe: "IP Address or hostname to connect to.",
    required: true,
    default: "127.0.0.1"
  },
  port: {
    alias: "p",
    default: 1337,
    describe: "Port number to connect to"
  }
};