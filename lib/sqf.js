"use strict";

var util = require("util");
var Type = require('type-of-is');


/**
 * Converts a JavaScript object to a string that can be passed through
 * call compile <...> in SQF
 *
 * Most JavaScript data types map nicely to SQF except Objects.
 *
 * Objects here are mapped to the following SQF data structure
 *  {[["key1", "value1"], ["key2", "value2"]]}
 *
 * @param data
 * @returns {string}
 */
exports.fromJS = function(data) {
  if (data === undefined || data === null) {
    return "nil";
  }

  if (data instanceof Buffer) {
    return exports.fromJS(data.toString());
  }

  if (typeof data === "string") {
    //escape existing double quotes
    return '"' + data.replace(/\"/g, '""') + '"';
  }

  if (data instanceof Array) {
    var elements = [];
    data.forEach(function(e, i) {
      elements.push(exports.fromJS(e));
    });

    return "[" + elements.join((", ")) + "]";
  }

  if (typeof data === "boolean") {
    return "(" + (data? "true": "false") + ")";
  }

  if (typeof data == "number") {
    return "(" + data.toString() + ")"
  }

  //else it must be an object

  var keys = Object.keys(data);


  var entry_set = [];
  keys.forEach(function(e, i){
    var sqf_key = exports.fromJS(e);
    var sqf_value = exports.fromJS(data[e]);

    if (sqf_key == "nil") {
      return;
    };

    var entry =  '[' + sqf_key + ',' + sqf_value + ']';
    entry_set.push(entry);
  });


  var sqf_object = '{[' + entry_set.join(",") + ']}';

  return sqf_object;
};



/**
 * Improved functionality over typeof operator
 */
var typeof2 = function(data) {
  return Type.string(data).toLowerCase();
};

/*
 * Reformats a JSON Object that was created from raw SQF, to the actual JSON
 */
var rawReformat = function(raw) {
  var type = typeof2(raw);

  //JSON Number
  if (type == "number") {
    return raw;
  }

  //JSON Boolean
  if (type == "boolean") {
    return raw;
  }

  //JSON Object
  if (type == "array" && typeof2(raw[0]) == "object") {
    var result = {};
    if (!util.isArray(raw[1])) {
      return result;
    }
    raw[1].forEach(function(pair) {
      if (!util.isArray(pair) || pair.length == 0) {
        return;
      }
      var key = rawReformat(pair[0]);
      if (!key) {
        return;
      }
      result[key] = rawReformat(pair[1]);
    });
    return result;
  }

  //JSON String
  if (type == "array" && typeof2(raw[raw.length-1]) == "object") {
    var string;
    if (util.isArray(raw[0])) {
      string = (new Buffer(raw[0])).toString();
    }
    return string;
  }

  //JSON Array
  if (type == "array") {
    for(var i = 0; i < raw.length; i++) {
      raw[i] = rawReformat(raw[i]);
    }
    return raw;
  }

  return null;
};

exports.isRaw = function(text) {
  return typeof text == "string" && (text.indexOf("RAW:") == 0);
};

var setExMsg = function(ex, message) {
  if (ex.message) {
    ex.message = message + ex.message;
  }
  else {
    ex.message = message;
  }
  return ex;
};

/**
 * Converts raw SQF text, to an actual JSON Object
 *
 * Since raw SQF has trouble with escaping strings, and does not have the concetps of hashes at all,
 * this function assumes that certain patterns in the raw SQF have special meaning
 *
 * Raw SQF String
 * ---------------
 *
 *  e.g.  [[183,35,67,56,445],{}]
 *
 * Here a string is reprenseted by an array where the last element is an empty SQF code block
 * The first element of the array contains the actual string contents, formatted as an array of octets
 *
 * Raw SQF Hash 
 * -------------
 * eg.  [{}, 
 *        [
 *          [key1,value1], 
 *          [key2,value2]
 *        ]
 *      ]
 *
 * Here a hash is represented by an array where the first element is an empty SQF code block
 *
 * The actual hash keys, and values are contained in the second element, as an array of key-value pairs
 *
 * Note that the keys, and values themselves follow the same raw SQF formatting rules.
 *
 * For example,  the following raw hash:
 * 
 *   [{},
 *     [
 *       [[[108,101,101,116],{}]  ,1337]
 *     ]
 *    ]
 *
 *  is equivalent to this JSON:
 *
 *   {
 *     "leet": 1337
 *   }
 *
 * Raw Nil 
 * -------
 * 
 *  e.g.  {nil}
 *
 * 
 *
 */

exports.rawToJSON = function(text) {
  var original = text;
  text = text.substring(4);
  text = text.replace(/(?:\{nil\}|nil|any|nothing|anything)/gi, "null");
  text = text.replace(/(?:-1\.#INF|-1\.#IND|-nan|-1\.#QNAN|1\.#QNAN)/gi, "0");
  var parsed;
  try {
    parsed = JSON.parse(text);
  }
  catch(ex) {
    throw setExMsg(ex, "RAW SQF request was not parsable as JSON");
  }

  if (!util.isArray(parsed)) {
    throw new Error("RAW SQF request was not an array");
  }

  return rawReformat(parsed);
};


