"use strict";

var net = require("net");
var log = require("util-logging");
var sqf = require( "./sqf.js");

var logger = new log.ConsoleLogger().setLevel(log.Level.INFO);
var sock = exports;

var handle_connection = function(ctx, callback) {
  logger.fine("Client(" + ctx.socketId + "): handle connection");

  //initial state
  ctx.state = 0;

  ctx.headerSz = 4;
  ctx.headerBuf = new Buffer(0);

  ctx.dataSz = 0;
  ctx.dataBuf = new Buffer(0);

  /**
   * 0 - waiting for size
   * 1 - waiting for packet
   */

  var data_machine = function(state, buf) {
    logger.finer("Client(" + ctx.socketId + "): state=" + state +", buf.length=" + buf.length);
    if (state == 0) {
      var needBytes = ctx.headerSz - ctx.headerBuf.length;
      logger.finer("Client(" + ctx.socketId + "): needBytes=" + needBytes);
      if (buf.length < needBytes) {
        ctx.headerBuf = Buffer.concat([ctx.headerBuf, buf]);
        return state;
      }

      if (buf.length == needBytes) {
        ctx.headerBuf = Buffer.concat([ctx.headerBuf, buf]);
        var size = ctx.headerBuf.readUInt32BE(0);
        ctx.headerBuf = new Buffer(0);
        logger.finer("Client(" + ctx.socketId + "): size=" + size);

        if (size == 0) {
          //special case for zero-length data
          logger.finer("Client(" + ctx.socketId + "): zero-length packet");
          ctx.dataBuf = new Buffer(0);

          ctx.dataSize = 0;
          state = 0;
          return state;
        }

        //set the next state
        ctx.dataSz = size;
        state = 1;
        return state;
      }

      if (buf.length > needBytes) {
        ctx.headerBuf = Buffer.concat([ctx.headerBuf, buf.slice(0, needBytes)]);
        var size = ctx.headerBuf.readUInt32BE(0);
        logger.finer("Client(" + ctx.socketId + "): size=" + size);

        ctx.headerBuf = new Buffer(0);


        if (size == 0) {
          //special case for zero-length data
          logger.finer("Client(" + ctx.socketId + "): zero-length packet");
          ctx.dataBuf = new Buffer(0);

          ctx.dataSize = 0;
          state = 0;

          //invoke the state machine with the remaining data
          return data_machine(state, buf.slice(needBytes));
        }

        //set the next state
        ctx.dataSz = size;
        state = 1;

        //invoke the state machine with the remaining data
        return data_machine(state, buf.slice(needBytes));
      }

      //should never reach here
      logger.severe("protocol error, unknown next state: " + state);
      return state;
    }

    if (state == 1) {
      var needBytes = ctx.dataSz - ctx.dataBuf.length;
      logger.finer("Client(" + ctx.socketId + "): needBytes=" + needBytes);
      if (buf.length < needBytes) {
        ctx.dataBuf = Buffer.concat([ctx.dataBuf, buf]);
        return state;
      }

      if (buf.length == needBytes) {
        ctx.dataBuf = Buffer.concat([ctx.dataBuf, buf]);
        callback(undefined, ctx.dataBuf);
        ctx.dataSz = 0;
        ctx.dataBuf = new Buffer(0);

        //set the next state
        state = 0;
        return state;
      }

      if (buf.length > needBytes) {
        ctx.dataBuf = Buffer.concat([ctx.dataBuf, buf.slice(0, needBytes)]);
        callback(undefined, ctx.dataBuf);
        ctx.dataSz = 0;
        ctx.dataBuf = new Buffer(0);

        //set the next state
        state = 0;

        //invoke the state machine with the remaining data
        return data_machine(state, buf.slice(needBytes));
      }

      //should never reach here
      logger.severe("Protocol error, unknown next state: " + state);
      return state;
    }
  };

  ctx.socket.on('data', function(buf) {
    logger.finer("Client(" + ctx.socketId + "): Event(data): buf.length=" + buf.length);
    ctx.state = data_machine(ctx.state, buf);
  });

};

var clients = [];
var cleanupClient = function(socket) {
  if (!socket) {
    return;
  }
  var index = clients.indexOf(socket);
  if (index == -1) {
    return;
  }

  clients.splice(index, 1);
};

var socketId = function(socket) {
  if (!socket) {
    return "unknown";
  }

  return socket.remoteAddress + ":" + socket.remotePort;
};

var ex2buf = function(ex) {
  var response = undefined;
  if (ex instanceof Error) {
    logger.severe(ex.message, ex);
    response = [true, "ERROR: " + ex.message];
  }
  else  {
    response = [true, ex];
  }

  var text_response;
  if (ex.accept == "application/json") {
    text_response = JSON.stringify(response);
  }
  else {
    text_response = sqf.fromJS(response);
  }

  return new Buffer(text_response);
};

exports.listen = function (host, port, callback) {
  var server = net.createServer(function (socket) {
    var ctx = {socket: socket, socketId: socketId(socket)};


    logger.info("Client(" + ctx.socketId + "): Event(connected)");
    clients.push(socket);

    socket.on('end', function() {
      logger.finer("Client(" + ctx.socketId + "): Event(end)");
      cleanupClient(ctx.socket);
    });

    socket.on('error', function (ex) {
      logger.warning("Client(" + ctx.socketId + "): Event(error): errno=" + ex.errno);
      if (ex.errno === "ECONNRESET") {
        cleanupClient(ctx.socket);
      }
    });

    handle_connection(ctx, function (err, inputBuf) {
      if (global.sock_rpc.argv["trace-recv"] && inputBuf) {
        console.log("recv: " + inputBuf.toString());
      }

      var rpc_ctx = {outputPending: true};
      callback.apply(ctx, [err, inputBuf, function (err, outputBuf) {
        if (err) {
          outputBuf = ex2buf(err);
        }
        logger.finer("Client(" + ctx.socketId + "): Event(response)");
        if (rpc_ctx && !rpc_ctx.outputPending) {
          return;
        }
        var sizeBuf = new Buffer(4);
        sizeBuf.writeUInt32BE(outputBuf.length, 0);
        logger.finer("Client(" + ctx.socketId + "): Event(response): outputBuf.length=" + outputBuf.length);

        if (global.sock_rpc.argv["trace-send"] && outputBuf) {
          console.log("send: " + outputBuf.toString());
        }

        ctx.socket.write(sizeBuf);
        ctx.socket.write(outputBuf);
        rpc_ctx.outputPending = undefined;
      }]);
    });

  });

  server.on('error', function(ex) {
    if (ex.errno === "EADDRNOTAVAIL") {
      logger.severe("Could not create server socket on specified host and port. Address is not available");
      return;
    }
    else {
      logger.severe(ex.message, ex);
    };
  });

  server.on('listening', function() {
    var address = server.address();
    logger.info("Server is now listening on: %j", address);
  });

  server.listen(port, host);
};


exports.clients = function() {return clients};

exports.setLogger = function (newLogger) {
  logger = newLogger;
};