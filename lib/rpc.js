"use strict";

var sqf = require(__dirname + "/sqf.js"),
  sock = require(__dirname + '/sock.js'),
  Client = require("./client.js"),
  log = require("util-logging"),
  logger = new log.ConsoleLogger().setLevel(log.Level.INFO),
  util = require("util");


exports.options = {
  host: {
    default: "localhost",
    describe: "IP address to listen on for client stats requests"
  },
  "trace-recv": {
    default: false,
    describe: "Trace all messages received before parsing"
  },
  "trace-send": {
    default: false,
    describe: "Trace all messages sent to remote clients"
  },
  "trace-req": {
    default: false,
    describe: "Trace all requests after parsing"
  },
  port: {
    default: 1337,
    describe: "port number to listen on for client stats requests"
  }
};




var functionName = function(fun) {
  if (typeof fun !== "function") {
    return;
  }

  var ret = fun.toString();
  ret = ret.substr('function '.length);
  ret = ret.substr(0, ret.indexOf('('));
  return ret;
};

var registry = {};

exports.setLogger = function (newLogger) {
  logger = newLogger;
  sock.setLogger(logger);
};

exports.register = function (p1, p2) {
  if (typeof p1 == "object") {
    for (var i in  p1) {
      if (p1.hasOwnProperty(i)) {
        var func = p1[i];
        if (typeof func === "function") {
          logger.info("Registering function \"" + i + "\"")
          registry[i] = func;
        }
      }
      return;
    }
  }

  if (p1 && p2 && typeof p1 == "string" && typeof p2 === "function") {
    logger.info("Registering function \"" + p1 + "\"")
    registry[p1] = p2;
    return;
  }
};


var handler = function(err, buf, callback) {
  try {
    if (err) {
      throw err;
    }

    var text = buf.toString();
    var request;
    try {
      if (sqf.isRaw(text)) {
        request = sqf.rawToJSON(text);
      }
      else {
        request = JSON.parse(buf.toString());
      }
    }
    catch(ex) {
      logger.severe("buf = %s", text);
      throw ex;
    }

    if (!request) {
      logger.severe("buf = %s", text);
      throw new Error("Request buffer could not be parsed either as pure JSON, or Raw SQF");
    }

    if (global.sock_rpc.argv["trace-req"]) {
      console.log("req: " + JSON.stringify(request));
    }

    var accept = request["accept"];
    var method_name = request["method"];

    if (!method_name || typeof method_name !== "string") {
      throw new Error("method name not valid");
    }

    var method = registry[method_name];
    if (!method || typeof method !== "function") {
      throw new Error("method \""+ method_name+"\" not found");
    }

    var params = request.params = request.params || [];
    if (!params || !(params instanceof Array)) {
      params = [];
    }

    params.push(function(err, result) {
      try {

        if (err && util.isError(err)) {
          throw err;
        };

        var response = [false, result];
        var text_response;
        if (accept && accept == "application/json") {
          text_response = JSON.stringify(response);
        }
        else {
          text_response = sqf.fromJS(response);
        }

        callback(undefined, new Buffer(text_response));
      }
      catch(ex) {
        ex.accept = accept;
        callback(ex);
      }
    });

    method.apply(this,params);
  }
  catch(ex) {
    ex.accept = accept;
    callback(ex);
  }
};

exports.listen = function(p1, p2) {

  var argHost = undefined;
  var argPort = undefined;

  if (p1 && p2) {
    //With two arguments arg1 is the host, and arg2 is the port
    argHost = p1;
    argPort = p2;
  }
  else if (p1 && !p2) {
    //With one argument , arg1 is the port
    argPort = p1;
  };

  var argv = require("yargs")
    .options(exports.options)
    .help("help")
    .alias("help", "h")
    .argv;

  global.sock_rpc = {argv: argv};

  var host = argHost || argv["host"] || "localhost";
  var port = argPort || parseInt(argv["port"]) || 0;

  sock.listen(host, port, handler, argv);
};

exports.Client = Client;
