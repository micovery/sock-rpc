var util = require("util"),
    net = require("net"),
    log = require("util-logging"),
    events = require('events');

var logger = new log.ConsoleLogger().setLevel(log.Level.INFO);


var Client = function(options) {
  var self = this;
  (self.super_ = Client.super_).call(self);

  options = options || {};
  self.host = options.host || "127.0.0.1";
  self.port = options.port || 1337;
};

util.inherits(Client, Object);


Client.prototype.defaultCallback = function(args, cb) {
  if (!cb || typeof cb !== "function") {
    cb = function() {};
  }

  if (!args || !util.isArray(args)) {
    return [cb];
  }

  if (typeof args[args.length -1]  !== "function") {
    args.push(cb);
  }

  return args;
};

Client.prototype.disconnect = function() {
  var self = this;
  var args = Array.prototype.slice.call(arguments, 0);
  var callback = self.defaultCallback(args).pop();

  if (!self.client) {
    return callback(new Error("Client not connected"));
  }

  self.client.on('end', callback);
  self.client.end();
  //self.client.destroy();

  delete self.client;
};

Client.prototype.connect = function() {
  var self = this;
  var args = Array.prototype.slice.call(arguments, 0);
  var callback = self.defaultCallback(args).pop();


  if (self.client) {
    return callback( new Error("Connection already established"));
  }

  var client = net.connect(self.port, self.host);
  self.client = client;

  /**
   * State machine
   * 0 - waiting for packet header
   * 1 - waiting for packet data
   */
  self.state = 0;
  self.headerSz = 4;
  self.dataSz = 0;

  self.buffer = new Buffer(0);

  client.on("connect", function() {
    callback(undefined);
  });

  client.on("error", function(err) {
    delete self.client;
    callback(err);
  });

  self.callbacks = [];

  var handleData = function(data) {
    logger.finest("data.length = %s", data.length);
    logger.finest("state = %s", self.state);
    if (self.state == 0) {
      var bytesNeeded = self.headerSz - self.buffer.length;
      logger.finest("bytesNeeded = %s", bytesNeeded);
      if (data.length == bytesNeeded) {
        self.dataSz = Buffer.concat([self.buffer, data]).readUInt32BE(0);
        self.buffer = new Buffer(0);
        self.state = 1;

        //ignore the packet if the data-size is 0
        if (self.dataSz == 0) {
          self.state = 0;
        }
      }
      else if (data.length < bytesNeeded) {
        self.buffer = Buffer.concat(self.buffer, data);
        self.state = 0;
      }
      else if (data.length > bytesNeeded) {
       handleData(data.slice(0, bytesNeeded));
       handleData(data.slice(bytesNeeded));
      }
      return;
    }

    if (self.state == 1) {
      var bytesNeeded = self.dataSz - self.buffer.length;
      logger.finest("bytesNeeded = %s", bytesNeeded);

      if (data.length == bytesNeeded) {
        var packet = Buffer.concat([self.buffer, data]).toString();
        logger.finest("Received packet: " + packet);
        var callback = self.callbacks.shift();
        if (!callback) {
          logger.severe("Un-requested packet received: %s");
        }
        else {
          callback(undefined, packet);
        }

        self.buffer = new Buffer(0);
        self.dataSz = 0;
        self.state = 0;
      }
      else if (data.length < bytesNeeded) {
        self.buffer = Buffer.concat(self.buffer, data);
        self.state = 1;
      }
      else if (data.length > bytesNeeded) {
        handleData(data.slice(0, bytesNeeded));
        handleData(data.slice(bytesNeeded));
      }
    }
  };

  client.on("data", handleData);
};



Client.prototype.sock_rpc = function(/* String */method, /* ...* */params, /* Function */callback) {
  var self = this;
  if (!self.client) {
    throw new Error("Client not connected");
  };

  var args = Array.prototype.slice.call(arguments, 0);
  callback = self.defaultCallback(args).pop();
  method = args.shift();
  params = args.shift();


  var packet_data, packet_header;
  var request = {
    "method": method,
    "params": params,
    "accept": "application/json"
  };

  packet_data = new Buffer(JSON.stringify(request));
  (packet_header = new Buffer(4)).writeUInt32BE(packet_data.length, 0);


  self.client.write(packet_header);
  self.client.write(packet_data);
  self.callbacks.push(callback);
};

module.exports = Client;